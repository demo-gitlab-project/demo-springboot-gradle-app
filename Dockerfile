FROM azul/zulu-openjdk:21
VOLUME /tmp
ADD build/libs/demo-app-0.0.1-SNAPSHOT.jar demo-app-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","demo-app-0.0.1-SNAPSHOT.jar"]